#!/bin/sh
grep-dctrl -n -s Package-List -w -F Build-Depends,Build-Depends-Indep "python3-mock" /var/lib/apt/lists/*_debian_dists_unstable_*_Sources | grep . | grep -v ' doc ' | grep -v '\-common' | awk '{print $1}' | sort -u > src
grep-dctrl -n -s Package -w -F Pre-Depends,Depends,Recommends "python3-mock" /var/lib/apt/lists/*_debian_dists_unstable_*_binary-amd64_Packages | sort -u > bin
