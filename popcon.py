#!/usr/bin/python3

# wget http://popcon.debian.org/by_inst.gz --no-clobber

import gzip

import debianbts as bts

import apt
import apt_pkg
apt_pkg.init()
SOURCES = apt_pkg.SourceRecords()
CACHE = apt.Cache()


def get_popcon() -> dict[str, int]:
    popcon: dict[str, int] = dict()
    with gzip.open('by_inst.gz', 'rb') as f:
        file_content = f.read().decode('latin1')

        for line in file_content.split('\n'):
            if not line or line[0] in ('#','-'):
               continue
            package, score = line.split()[1:3]
            popcon[package] = int(score)
    return popcon

popcon = get_popcon()

def get_todo() -> dict[str, int]:
    todo: dict[str, int] = dict()
    with open('src', 'r') as f:
        for package in f:
            package = package.strip('\n')
            todo[package] = popcon.get(package, 0)
    with open('bin', 'r') as f:
        for package in f:
            package = package.strip('\n')
            todo[package] = popcon.get(package, 0)
    return todo

todo = get_todo()
# not-a-bug 1064127
todo.pop('libjs-sphinxdoc', None)
todo.pop('python-sphinx', None)
todo.pop('python3-sphinx', None)

def get_patch() -> dict[str, str]:
    '''patch sent/existing upstream'''
    patch: dict[str, str] = dict()
    with open('pr', 'r') as fd:
        for line in fd:
            package, url = line.strip('\n').split(None, 1)
            patch[package] = url
    return patch

patch = get_patch()

def get_pending() -> dict[str, int]:
    pending: dict[str, int] = dict()
    rm_bug = bts.get_status(['1059933'])[0]
    assert rm_bug.subject == 'python-mock: RM python3-mock / tracker'
    for bug in bts.get_status(rm_bug.blockedby):
        if bug.archived:
            continue
        #print(bug.bug_num, bug.package, bug.affects)
        if bug.package == 'ftp.debian.org':
            if 'RM' in bug.subject:
                package = bug.affects[0].split(':')[-1]
                pending[package] = bug.bug_num
        elif bug.package.startswith('src:'):
            source = bug.package.split(':')[1]
            if SOURCES.lookup(source):
                for binary in SOURCES.binaries:
                    pending[binary] = bug.bug_num
            else:
                # source not found (???):
                # - datalad
                # - libabigail
                # - python-sure

                # fallback code
                if source.startswith('python-'):
                    maybe_name = source.replace('python-', 'python3-')
                else:
                    maybe_name = 'python3-' + source
                pending[maybe_name] = bug.bug_num
        else:
            pending[bug.package] = bug.bug_num
    return pending

pending = get_pending()

# recheck because of buggy Sources list with old data #1064127
for p in sorted(todo):
    try:
        package = CACHE[p]
    except KeyError:
        # "The cache has no package named 'python-sphinx'"
        todo.pop(p)
        continue

    still_needed = False
    for dependencies in package.candidate.dependencies:
        for dependency in dependencies:
            if dependency.name == 'python3-mock':
                still_needed = True
    if still_needed:
        continue

    source = package.candidate.source_name
    if not SOURCES.lookup(source):
        continue

    still_needed = False
    for relationship, depends in SOURCES.build_depends.items():
        for dependency in depends:
            name, version, operator_ = dependency[0]
            if name == 'python3-mock':
                still_needed = True
    if not still_needed:
        todo.pop(p)

for k, v in sorted(todo.items(), key=lambda item: (item[1], item[0])):
    if k in pending:
        line = '%-40s %-6s #%s' % (k, v, pending[k])
    elif k in patch:
        line = '%-40s %-6s (%s)' % (k, v, patch[k])
    else:
        line = '%-40s %-6s' % (k, v)
    print(line.rstrip())
